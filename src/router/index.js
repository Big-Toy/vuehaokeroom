import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter)

// 一级路由
import LayoutAndaxios from "@/views/LayoutAndaxios.vue"
import Login from "@/views/login.vue"
import Detail from "@/views/detail.vue"
import CityList from "@/views/CityList.vue"
import Collect from "@/views/collect.vue"
import Personal from "@/views/PersonalData.vue"
import RentOut from "@/views/GoToTheRentOut.vue"
import MapFindRoom from "@/views/MapFindRoom.vue"
import HouseGuanli from "@/views/HouseGuanLi.vue"  
import SearchFindRoom from "@/views/searchFindRoom.vue"  
// 二级路由
import My from "@/views/secend/My.vue"
import FindRoom from "@/views/secend/FindRoom.vue"
import News from "@/views/secend/News.vue"
import Head from "@/views/secend/Head.vue"
const router = new VueRouter({
  mode:"history",
  routes: [
    {
      path: "/", // 默认hash值路径
      redirect: "/layoutAndaxios/head" // 重定向到/home/article
      // 浏览器url中#后的路径被改变成/find-重新匹配数组规则
    },
    {
      path: "/login",
      component: Login
    },
    {
      path: "/citylist",
      component:CityList
    },
    {
      path:"/layoutAndaxios",
      component: LayoutAndaxios,
      children:[
        {
          path:"news",
          component: News,
        },
        {
          path:"my",
          component: My,
        },
        {
          path:"findroom",
          component: FindRoom,
        },
        {
          path:"head",
          component: Head,
        },
      ]
    },
    {
      path:"/detail",
      component: Detail,
    },
    {
      path:"/rentout",
      component: RentOut,
    },
    {
      path:"/personal",
      component: Personal,
    },
    {
      path:"/collect",
      component: Collect,
    },
    {
      path:"/mapfindroom",
      component: MapFindRoom,
    },
    {
      path:"/houseguanli",
      component: HouseGuanli,
    },
    {
      path:"/searchfindroom",
      component: SearchFindRoom,
    },
  ]
})

export default router
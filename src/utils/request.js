import axios from "axios";

// 引入本地存储
import {getItem,key} from "@/utils/storage"

const request = axios.create({
  baseURL: "http://liufusong.top:8080",
  timeout: 999999,
});

// 请求拦截器
request.interceptors.request.use(
  (config) => {
    const token = getItem(key) 
    config.headers.Authorization = token;
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

// 响应拦截器
request.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (err) => {
    return Promise.reject(err);
  }
);

// 抛出request
export default request;

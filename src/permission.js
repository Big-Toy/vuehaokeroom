import router from "./router";
import {getItem,key} from "@/utils/storage"

const list = ["/login"]
router.beforeEach((to,from,next)=>{
  const token = getItem(key)
  if(token) {
    // console.log(111);
    next()
  } else {
    // console.log(222);
    if(list.includes(to.path)) {
      next()
      // console.log(3333);
    } else {
      // console.log(4444);
      next("/login")
    }
  }
})
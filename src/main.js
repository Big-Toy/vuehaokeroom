import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 引入百度地图
import BaiduMap from 'vue-baidu-map'
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'hN44y2qParmQaNoukdZMzswj4G2UmeZM'
})

import "@/permission.js"
// 引入 vant.js
import "@/api/vant.js"

Vue.config.productionTip = false


const el =  new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

// console.log(el);

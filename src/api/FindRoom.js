import request from "@/utils/request.js"
// 获取找房列表
export const getFindRoomListApi = (params) => {
  return request({
    url: "/admin/houses",
    Params:params
  })
}

// 获取具体房屋详情
export const getFindRoomDetailApi = (id) => {
  return request({
    url: `/houses/${id}`,
  })
}

// 获取收藏
export const CollectListApi = () => {
  return request({
    url: "/user/favorites",
  })
}

// 获取猜你喜欢
export const GuessApi = (params) => {
  return request({
    url: "/admin/houses",
    Params:params
  })
}

// 获取 My
export const PersonalApi = () => {
  return request({
    url: "/user",
  })
}
// 添加收藏  
export const addLikeListApi = (id) => {
  return request({
    method: "POST",
    url: `/user/favorites/${id}`,
  })
}
// 删除收藏  
export const delLikeListApi = (id) => {
  return request({
    method: "DELETE",
    url: `/user/favorites/${id}`,
  })
}
// 登录  
export const loginApi = (data) => {
  return request({
    method: "post",
    url: "/user/login",
    data
  })
}
// 发布房源  
export const showHouseApi = (body ) => {
  return request({
    method: "post",
    url: "/user/houses",
    body 
  })
}
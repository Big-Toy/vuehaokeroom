import Vue from "vue";

import {
  Uploader ,
  Button,
  Icon,
  Tabbar,
  DropdownMenu,
  DropdownItem,
  TabbarItem,
  Cell,
  CellGroup,
  Form,
  Field,
  NavBar,
  Grid,
  GridItem,
  Search,
  Swipe,
  SwipeItem,
  Lazyload,
  List,
  IndexBar,
  IndexAnchor,
  Popup,
  Picker,
  Toast,
} from "vant";
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Button);
Vue.use(Icon);
Vue.use(Form);
Vue.use(Field);
Vue.use(NavBar);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Search);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Lazyload);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(List);
Vue.use(IndexBar);
Vue.use(IndexAnchor);
Vue.use(Popup);
Vue.use(Picker);
Vue.use(Uploader );
Vue.use(Toast);







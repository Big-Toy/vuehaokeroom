import request from "@/utils/request.js"
// 获取城市列表数据
export const getCityList = (params) => {
  return request({
    url: "/area/city",
    params:params
  })
}
// export const getMapList = () => {
//   return request({
//     url: "/area/info",
//   })
// }
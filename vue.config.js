const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            // 直接覆盖变量
            blue: '#21b97a',
            // 'button-primary-background-color': '#FA6D1D'
            // 'button-primary-border-color': '#FA6D1D'
            'nav-bar-title-text-color':'#fff'
          }
        }
      }
    }
  }
})
